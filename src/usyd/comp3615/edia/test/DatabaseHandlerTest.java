package usyd.comp3615.edia.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import junit.framework.Assert;
import usyd.comp3615.edia.db.Consumption;
import usyd.comp3615.edia.db.DatabaseHandler;
import usyd.comp3615.edia.db.FavouriteItem;
import usyd.comp3615.edia.db.Item;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

public class DatabaseHandlerTest extends AndroidTestCase {
	//android.test.ActivityUnitTestCase<DatabaseHandler>
	private DatabaseHandler db;
	private SQLiteDatabase sql;
	private RenamingDelegatingContext context;
	
	
	@Override
	public void setUp() throws Exception{
		super.setUp();
		context = new RenamingDelegatingContext(getContext(), "test_");
		db = new DatabaseHandler(context);
		sql = db.getReadableDatabase();
	}
	
	public void tearDown() throws Exception {
		db.close();
		super.tearDown();
	}
	
	
	
	
	/**
	 * Check if the database is
	 */
	
	public void testDatabaseInAssetsFolder() {
		InputStream myInput;
		/*
		try {
			myInput = context.getAssets().open(DatabaseHandler.DB_NAME);
			Assert.assertEquals(1910784, myInput.available());
		} catch (IOException e) {
			Assert.fail("Couldn't find file in assets");
		}*/
	}
	
	/**
	 * Check if database is successfully created locally
	 */
	public void testCreateDatabase() {
		db.createDataBase();
		Assert.assertNotNull(db);
		Assert.assertEquals(true, db.createDataBase());
	}
	
	public void testDatabaseCanBeAccessed() {
		SQLiteDatabase checkDB;
		Assert.assertNotNull(SQLiteDatabase.openDatabase(DatabaseHandler.DB_PATH + DatabaseHandler.DB_NAME, null, SQLiteDatabase.OPEN_READONLY));
		
	}
	

	
	
	public void canGetWriteableReadableDB() {
		Assert.assertNotNull(db.getReadableDatabase());
		Assert.assertNotNull(db.getWritableDatabase());
	}
	
	public void testCanGetItems() {
		Assert.assertNotNull(db.getReadableDatabase());
		//Assert.assertEquals(6, db.getNames("carrot").size());
	}
	
	public void testGetTables() {
		Assert.assertEquals(7, db.getTables());
	}
	
	public void testIsLoggedIn() {
		Assert.assertFalse(db.isLoggedIn());
		sql.execSQL("INSERT INTO login (_id, email) VALUES ('1', 'tom@puric.com')");
		Assert.assertTrue(db.isLoggedIn());
		String s[] = {"1"};
		sql.delete("login", "_id = ?", s);
		//db.logout();
		Assert.assertFalse(db.isLoggedIn());
		db.logout();
		Assert.assertFalse(db.isLoggedIn());
		
	}
	
	public void testLogin(String email) {
		
	}
	
	public void testLogout() {
		
	}
	
	public void testAddItem(Item item) {
		
	}
	
	public void testGetItem(int id) {
		
	}
	
	public void testGetFavouriteItem(int id) {
		
	}
	
	public void testGetBrands(int itemId) {
		
	}
	
	public void testGetNamesCal(String str) {
		
	}
	
	public void testGetAllItems() {
		
	}
	
	public void testGetItemsCount() {
		
	}
	
	public void testUpdateItem(Item item) {
		
	}
	
	public void testDeleteItem(Item item) {
		
	}
	
	public void testGetAllFavourites() {
		
	}
	
	public void testAddFavourite(Item item, String name) {
		
	}
	
	public void testAddFavourite(FavouriteItem fitem) {
		
	}
	
	public void testDeleteFavourite(Item item) {
		
	}
	
	public void testAddConsumption(int itemID, String name, long quantity, String mealType) {
		
	}
	
	public void testCheckForConsumption(int itemID, String mealType) {
		
	}
	
	public void testGetAllConsumptionItems(int i, String mealType) {
		
	}
	
	public void testDeleteConsumption(Consumption c) {
		
	}
	
	public void testUpdateConsumption(int id, int quantity) {
		
	}
	
	public void testGetMeasures(Item item) {
		
	}
	
	public void testGetMeasureID(Item item) {
		
	}
	
	public void testAddRecipe(ArrayList<Consumption> items, String name, String description) {
		
	}
	
	public void testGetAllRecipes() {
		
	}
	
	public void testGetRecipe() {
		
	}
	

}

